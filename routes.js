/*
	CREATE A ROUTE
		"/greeting"
*/
const http = require("http");
const port = 4000;
let url = require("url");
const server = http.createServer((request, response) => {

		// Accessing the "greeting" route returns a message of "Hello World"
		if(request.url == '/greeting'){
			response.writeHead(200, {"Content-Type":"text/plain"})
			response.end("Hello from the other world! 32131")
		}
		else if(request.url == '/homepage'){
			response.writeHead(200, {"Content-Type":"text/plain"})
			response.end('Welcome Home')
		}
		else{
			response.writeHead(200, {"Content-Type":"text/plain"})
			response.end('Page is not available')
		}
})

// uses the "server" and "port" variables created above
server.listen(port)

console.log(`Server now accesible at localhost:${port}`)