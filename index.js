// HTTP (HyperText Transfer Protocol)
// Use the "require" directive to load the HTTP module of node JS
// A 'module' is software component or part of prgoram that contains one or more routines.
//  The 'http module' lets Node.js transfer data using the HTTP.
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (nodeJS/ExpressJS applications) communicate exchanging individual messages.

// REQUEST - messages sent by the client (usually in a Web browser)
// RESPONSES - messages sent by the server as an answer to the client

let http = require("http")

// CREATE A SERVER
/* 
	- The http module has a createServer()method that accept a function as an argument and allows server creation

	- The arguments passed in the function are request & response objects (data type) that contain methods that allow us to receive requests from the client and set the responses back

	- Using the module's createServer() Method, we can create an HTTP server that listens to the requests on a specified port and gives back to the client
*/

// Define the Port number that the server will be listening to

http.createServer(function(request,response){
		response.writeHead(200,{"Content-Type": "text/plain"});
		response.end(`Welcome to my World
-viper`);
}).listen(4000)

console.log("Server running at localhost: 4000")

// A port is virtual point where network connections start and end

// Send a response back to the client
// Use the writeHead() method
// Set a status code for the response - a 200 - OK

// Inputting the command tells our device to run node js
// node index.js

